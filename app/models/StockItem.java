package models;

import java.util.*;
import models.Product;


public class StockItem {
    public Warehouse warehouse; // truong quan he noi voi Warehouse
    public Product product; // truong quan he noi voi Product
    public Long quantity;
    public String toString() {
        return String.format("$d %s", quantity, product);
    }
}